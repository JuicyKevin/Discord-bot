**Developer Guide**

Config.json:
    - This file is omitted, since it will contain sensitive data, such as the bot's secret token
    - Below is a general structure of the file, so you can add it locally and get your own bot working:
```
{
    "token": "*your token*"
}
```
